package org.kampertnauta.offsetplanner.solver;

import java.util.HashMap;
import java.util.List;

import org.kampertnauta.offsetplanner.domain.Layout;
import org.kampertnauta.offsetplanner.domain.OrderItem;

public class CachedGuilliotineFitter {
	
	private HashMap<String, Boolean> cacheMap;
	
	public CachedGuilliotineFitter(){
		cacheMap = new HashMap<String, Boolean>();
	}
	
	public Boolean checkIfOrderItemsFitOnLayout(Layout layout, List<OrderItem> orderItems){
		return false;
	}
}