package org.kampertnauta.offsetplanner.app;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.kampertnauta.offsetplanner.domain.LayoutSolution;
import org.kampertnauta.offsetplanner.domain.OrderItem;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.core.impl.score.director.ScoreDirectorFactory;

public class OffsetPlanner {
	public static ScoreDirector scoreDirector;
	
	public static void main(String[] args) {
		 SolverFactory solverFactory = SolverFactory.createFromXmlResource("org/kampertnauta/offsetplanner/solver/OffsetPlannerSolverConfig.xml");
		 Solver solver = solverFactory.buildSolver();
		
		 ScoreDirectorFactory scoreDirectorFactory = solver.getScoreDirectorFactory();
		 scoreDirector = scoreDirectorFactory.buildScoreDirector();
		 
		 LayoutSolution unsolvedSolution = new LayoutSolution();
		 
		 scoreDirector.setWorkingSolution(unsolvedSolution);
		 for (OrderItem orderItem : unsolvedSolution.getOrderItems()){
			 System.out.println("Order item: " + orderItem.toString() + " Layout: " + orderItem.getLayout().toString());
		 }
		 System.out.println("Pre-Solver");
		 solver.solve(unsolvedSolution);
		 LayoutSolution finalSolution = (LayoutSolution)solver.getBestSolution();		
		 finalSolution.getScore().toString();
		 System.out.println("Post-Solver");
	}

}
