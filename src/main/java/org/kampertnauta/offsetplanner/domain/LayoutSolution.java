package org.kampertnauta.offsetplanner.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;


@PlanningSolution
public class LayoutSolution implements Solution<HardSoftScore>{

	private List<OrderItem> orderItems; 
	private List<Layout> layouts;
	
	private HardSoftScore score;
	
	public LayoutSolution(){
		this.orderItems = new ArrayList();
		this.layouts = new ArrayList();
		this.PopulateLayoutSolution();
	}
	
	@ValueRangeProvider(id = "layoutRange")
	public List<Layout> getLayouts() {
		return layouts;
	}

	@PlanningEntityCollectionProperty
	public List<OrderItem> getOrderItems(){
		return this.orderItems;
	}
	
	public void setOrderItems(List<OrderItem> orderItems){
		this.orderItems = orderItems;
	}
	
	public HardSoftScore getScore() {
		System.out.println("SCORE "+ score.toString() );
		return score;
	}

	public void setScore(HardSoftScore score) {
		this.score=score;
		
	}

	public Collection<? extends Object> getProblemFacts() {
		List<Object> facts = new ArrayList<Object>();
		
		facts.addAll(layouts);
		
		return facts;
	}
	
	private void PopulateLayoutSolution(){
		for(int i = 1; i <= 10; i++){
			OrderItem orderItem = new OrderItem(3 * i, 5 * i, 100 * i);
			Layout layout = new Layout(50, 50, i);
			orderItem.setLayout(layout);
			this.layouts.add(layout);
			this.orderItems.add(orderItem);
		}
	}
}
