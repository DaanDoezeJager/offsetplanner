package org.kampertnauta.offsetplanner.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
public class OrderItem {
	private Layout layout;
	
	private int width, height, quantity;
	
	private OrderItem(){
		
	}
	
	public OrderItem(int width, int height, int quantity){
		this.width=width;
		this.height=height;
		this.quantity=quantity;
	}
	
	public Integer getWidth() {
		return width;
	}

	public Integer getHeight() {
		return height;
	}
	
	@PlanningVariable(valueRangeProviderRefs = { "layoutRange" })
	public Layout getLayout() {
		return layout;
	}

	public void setLayout(Layout layout) {
		this.layout = layout;
	}

}
