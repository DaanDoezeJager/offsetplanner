package org.kampertnauta.offsetplanner.domain.solver;

import java.util.Comparator;

import org.kampertnauta.offsetplanner.domain.OrderItem;

public class OrderItemComparator implements Comparator<OrderItem>{
	public int compare(OrderItem arg0, OrderItem arg1) {
		int  orderItemResult = arg0.getHeight().compareTo(arg1.getHeight());
		if (orderItemResult != 0)
		{
			return orderItemResult;
		}
		else {
			orderItemResult = arg0.getWidth().compareTo(arg1.getWidth());
			return orderItemResult;
		}
	}

}
