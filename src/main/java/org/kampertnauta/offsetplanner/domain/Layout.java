package org.kampertnauta.offsetplanner.domain;

import java.io.Serializable;
import java.util.List;

//import org.kampertnauta.offsetplanner.domain.solver.OrderItemComparator;
import org.kampertnauta.offsetplanner.solver.score.FacturatieConsts;


public class Layout implements Serializable{	
	private int width;
	private int height;
	private int id;
	private double paper_weight;
	private double price_kilo_offset;
	private double sheetcut_thickness_mm;
	
	private FacturatieConsts consts;
	
	public Layout(int width, int height, int id){
		this.width = width;
		this.height = height;
		this.id = id;
		this.consts = new FacturatieConsts();
	}
	

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public int getId() {
		return id;
	}


	public boolean orderItemsFitGuilliotineOnLayout( List<OrderItem> orderItems){
		//orderItems.sort(new OrderItemComparator());
		String arg = width + " " + height;
		for (OrderItem orderItem : orderItems){
			arg += orderItem.getHeight() + " " + orderItem.getWidth();
		}
		return false;
	}
	
	public double getLayoutCreationCost(List<OrderItem> orderItems){
		double sheet_weight = (width / 1000) * (height / 1000) * (paper_weight/1000);
		
		return 0;
	}
}
